import wisardpkg as wp
import numpy as np


def binarization(X_train, X_test, y_train, y_test, binarization_name):
    entry_size = len(X_train[0])
    binarizer = None
    if binarization_name[:2] == "th":
        concat = [*X_train, *X_test]
        mins = np.min(concat, axis=0)
        maxs = np.max(concat, axis=0)
        therm_size = int(binarization_name[2:])
        binarizer = wp.DynamicThermometer([therm_size] * entry_size, mins, maxs)
    elif binarization_name[:2] == "mt":
        binarizer = wp.MeanThresholding()
    else:
        raise Exception("Binarization not found!")

    train_ds = wp.DataSet()
    test_ds = wp.DataSet()

    for i in range(len(X_train)):
        train_ds.add(binarizer.transform(X_train[i]), y_train[i])

    for i in range(len(X_test)):
        test_ds.add(binarizer.transform(X_test[i]), y_test[i])

    return {"train": train_ds, "test": test_ds}


def get_imdb(num_words, binarization_name):
    return {
        "train": wp.DataSet(
            "new_techiques/imdb/{}/imdb_train_{}.wpkds".format(num_words, binarization_name)
        ),
        "test": wp.DataSet(
            "new_techiques/imdb/{}/imdb_test_{}.wpkds".format(num_words, binarization_name)
        ),
    }

