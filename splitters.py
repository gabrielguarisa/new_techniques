import random
import numpy as np


def split_ones_1(data, tuple_size, frac=1.0):
    random.shuffle(data)
    entry_size = len(data[0])
    num_tuples = int(entry_size / tuple_size)
    complete_addr = False
    num_images = int(len(data) * frac)
    if float(entry_size) % float(tuple_size) > 0:
        num_tuples += 1
        complete_addr = True

    final_tuples = [[] for _ in range(num_tuples)]
    valid_tuples = [[True for _ in range(num_tuples)] for _ in range(entry_size)]
    free_addr = [True for _ in range(entry_size)]

    for i in range(num_images):
        addr_one = []

        for j in range(entry_size):
            if data[i][j] == 1 and free_addr[j]:
                addr_one.append(j)

        random.shuffle(addr_one)

        for addr_1 in addr_one:
            for t in range(num_tuples):
                if valid_tuples[addr_1][t] == True and len(final_tuples[t]) <= (
                    tuple_size - 1
                ):
                    final_tuples[t].append(addr_1)
                    free_addr[addr_1] = False

                    for addr_2 in addr_one:
                        if addr_1 != addr_2:
                            valid_tuples[addr_2][t] = False

                    break

    left_ones = []
    for i in range(entry_size):
        if free_addr[i]:
            left_ones.append(i)

    random.shuffle(left_ones)

    i = 0
    for t in range(num_tuples):
        diff = tuple_size - len(final_tuples[t])
        if diff > 0:
            if len(left_ones) > 0:
                for j in range(i, i + diff):
                    new_j = j % len(left_ones)
                    final_tuples[t].append(left_ones[new_j])
            else:
                for j in range(i, i + diff):
                    new_addr = random.randint(0, entry_size)
                    while new_addr in final_tuples[t]:
                        new_addr = random.randint(0, entry_size)
                    final_tuples[t].append(new_addr)

        i = diff

        if i > len(left_ones):
            random.shuffle(left_ones)

    return final_tuples


def split_ones_2(data, tuple_size, frac=1.0):
    random.shuffle(data)
    entry_size = len(data[0])
    num_tuples = int(entry_size / tuple_size)
    complete_addr = False
    num_images = int(len(data) * frac)
    if float(entry_size) % float(tuple_size) > 0:
        num_tuples += 1
        complete_addr = True

    final_tuples = [[0 for _ in range(entry_size)] for _ in range(num_tuples)]
    used_addr = []

    for i in range(num_images):
        addr_one = []

        for j in range(entry_size):
            if data[i][j] == 1:
                addr_one.append(j)

        random.shuffle(addr_one)

        for addr_1 in addr_one:
            for t in range(num_tuples):
                if final_tuples[t][addr_1] >= 0:
                    for addr_2 in addr_one:
                        if addr_1 == addr_2:
                            final_tuples[t][addr_1] += 1
                        else:
                            final_tuples[t][addr_2] -= 1

                    used_addr.append(addr_1)
                    break

    mapping = []

    for i in range(num_tuples):
        mapping.append([])
        tuple_indexes = np.argsort(final_tuples[i])[-tuple_size:]
        for j in tuple_indexes:
            if final_tuples[i][j] > 0:
                mapping[i].append(j)

    left_ones = []
    for i in range(entry_size):
        if i not in used_addr:
            left_ones.append(i)

    random.shuffle(left_ones)

    i = 0
    for t in range(num_tuples):
        diff = tuple_size - len(mapping[t])
        if diff > 0:
            if len(left_ones) > 0:
                for j in range(i, i + diff):
                    new_j = j % len(left_ones)
                    mapping[t].append(left_ones[new_j])
            else:
                for j in range(i, i + diff):
                    new_addr = random.randint(0, entry_size)
                    while new_addr in mapping[t]:
                        new_addr = random.randint(0, entry_size)
                    mapping[t].append(new_addr)

        i = diff

        if i > len(left_ones):
            random.shuffle(left_ones)

    return mapping

