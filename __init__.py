import wisardpkg as wp

def valid_tuple_sizes(entry_size, max_value=64):
    valid_values = []
    for tuple_size in range(3, max_value):
        if entry_size % tuple_size == 0:
            valid_values.append(tuple_size)
    return valid_values


def get_mapping_score(mapping, train_ds, test_ds, monomapping=False):
    if monomapping:
        mapping = {"__monomapping__": mapping}

    wsd = wp.Wisard(3, mapping=mapping, monoMapping=monomapping)
    wsd.train(train_ds)
    return wsd.score(test_ds)


def get_random_score(tuple_size, train_ds, test_ds, monomapping=False):
    wsd = wp.Wisard(tuple_size, monoMapping=monomapping)
    wsd.train(train_ds)
    return wsd.score(test_ds)


def get_images_by_label(ds):
    images = {}
    for i in range(len(ds)):
        if ds.getLabel(i) not in images:
            images[ds.getLabel(i)] = []
        images[ds.getLabel(i)].append(ds[i])
    return images
